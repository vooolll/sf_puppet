while true; do
    echo "[mongooseIm]:"
    read input_variable


    if [ $input_variable == "log" ] ; then
        if [ -f /vagrant/sf_chat/dev/mongooseim_odbc_redis/log/ejabberd.log ]
            then
            vim /vagrant/sf_chat/dev/mongooseim_odbc_redis/log/ejabberd.log
        else
            echo "log file do not exist"
        fi
    elif [ $input_variable == "devrel" ] ; then
        cd /vagrant/sf_chat
        make devrel
    elif [ $input_variable == "rel" ] ; then
        cd /vagrant/sf_chat
        make rel
    elif [ $input_variable == "relclean" ] ; then
        cd /vagrant/sf_chat
        make relclean
    elif [ $input_variable == "devclean" ] ; then
        cd /vagrant/sf_chat
        make devclean
    elif [ $input_variable == "make" ] ; then
        cd /vagrant/sf_chat
        make
    elif [ $input_variable == "clean" ] ; then
        cd /vagrant/sf_chat
        rm -rf deps
    else 
        if [ -f /vagrant/sf_chat/dev/mongooseim_odbc_redis/log/ejabberd.log  ]
            then
            rm /vagrant/sf_chat/dev/mongooseim_odbc_redis/log/ejabberd.*
        fi
        rm /vagrant/sf_chat/dev/mongooseim_odbc_redis/log/erlang.*
        rm /vagrant/sf_chat/dev/mongooseim_odbc_redis/log/crash.*



        /vagrant/sf_chat/dev/mongooseim_odbc_redis/bin/mongooseimctl $input_variable
        sleep 3
        /vagrant/sf_chat/dev/mongooseim_odbc_redis/bin/mongooseimctl status
    fi
done