CREATE TABLE users
(
  username character varying(250) NOT NULL,
  password text NOT NULL,
  pass_details text,
  created_at timestamp without time zone NOT NULL DEFAULT now(),
  email_hash character varying(255),
  status character varying(255),
  CONSTRAINT users_pkey PRIMARY KEY (username)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO chat_app;


-- Table: privacy_list_data

-- DROP TABLE privacy_list_data;
CREATE TABLE privacy_list (
    username varchar(250) NOT NULL,
    name text NOT NULL,
    id SERIAL UNIQUE,
    created_at TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE privacy_default_list (
    username varchar(250) PRIMARY KEY,
    name text NOT NULL
);

CREATE TABLE privacy_list_data
(
  id bigint,
  t character(1) NOT NULL,
  value text NOT NULL,
  action character(1) NOT NULL,
  ord numeric NOT NULL,
  match_all boolean NOT NULL,
  match_iq boolean NOT NULL,
  match_message boolean NOT NULL,
  match_presence_in boolean NOT NULL,
  match_presence_out boolean NOT NULL,
  blocked_user_id character varying(255),
  reason character varying(35),
  CONSTRAINT privacy_list_data_id_fkey FOREIGN KEY (id)
      REFERENCES privacy_list (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE privacy_list_data
  OWNER TO chat_app;