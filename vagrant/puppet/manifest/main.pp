class apt_update {
		exec { "aptGetUpdate":
				command => "sudo apt-get update",
				path => ["/bin", "/usr/bin"],
				user => root,
		}
}

class othertools {
		package { "git":
			  ensure => latest,
			  require => Exec["aptGetUpdate"],
			  
		}

		package { "vim-common":
			  ensure => latest,
			  require => Exec["aptGetUpdate"],
		}

		package { "curl":
			  ensure => present,
			  require => Exec["aptGetUpdate"],
		}

		package { "htop":
				ensure => present,
				require => Exec["aptGetUpdate"],
		}
}


node "vagrant-ubuntu-trusty-64" {
		
		include apt_update
		include othertools
		include redis
		include 'erlang' 
		include beanstalkd
		php::ini { '/etc/php.ini':
  			display_errors => 'On',
  			memory_limit   => '2Gb',
		}

		include php::cli

		php::module { [ 'mcrypt' ]: }

		class { 'composer':
				command_name => 'composer',
				target_dir   => '/usr/local/bin'
		}


		class { 'nginx':	}

		nginx::resource::vhost { 'sf.lcl':
		    www_root            => '/var/www/sf/sf/public/',
		}

		class { 'postgresql::globals': }
		class { 'postgresql::server':
				ip_mask_allow_all_users    => '0.0.0.0/0',
				ipv4acls                   => ['host    all     postgres        127.0.0.1/32    trust'],
				listen_addresses           => '*',
				postgres_password          => '12345',
		}
		postgresql::server::db { 'springfruit_dev':
				user     => 'chat_app',
				password => postgresql_password('chat_app', '12345')
		}

		class { 'mongoose':
				host   		 => 'localhost',
				dbhost 		 => 'localhost',
				dbname 		 => 'springfruit_dev',
				dbport 		 => '5432',
				dbuser 		 => 'chat_app',
				dbpassword => '12345'
		}


}


