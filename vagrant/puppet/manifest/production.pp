class apt_update {
    exec { "aptGetUpdate":
        command => "sudo apt-get update",
        path => ["/bin", "/usr/bin"],
        user => root,
    }
}

class othertools {
    package { "git":
        ensure => latest,
        require => Exec["aptGetUpdate"],
        
    }

    package { "vim-common":
        ensure => latest,
        require => Exec["aptGetUpdate"],
    }

    package { "curl":
        ensure => present,
        require => Exec["aptGetUpdate"],
    }

    package { "htop":
        ensure => present,
        require => Exec["aptGetUpdate"],
    }
}


node "ip-172-31-28-133.us-west-1.compute.internal" {
    
    include apt_update
    include othertools
    include redis
    include 'erlang' 
    include beanstalkd

    php::ini { '/etc/php.ini':
        display_errors => 'On',
        memory_limit   => '256Gb',
    }

    include php::cli

    php::module { [ 'mcrypt' ]: }

    class { 'composer':
        command_name => 'composer',
        target_dir   => '/usr/local/bin'
    }


    class { 'nginx':  }

    nginx::resource::vhost { 'sf.lcl':
        www_root            => '/var/www/sf/sf/public/',
    }

    class { 'postgresql::globals': }
    class { 'postgresql::server':
        ipv4acls                   => ['host    all     postgres        127.0.0.1/32    trust'],
        listen_addresses           => "*",
        postgres_password          => 'loveyourmom',
    }




    postgresql::server::db { 'spring_fruit_production':
        user     => 'sf',
        password => postgresql_password('sf', 'loveyourdoughter')
    }


    postgresql::server::role { 'marat':
        password_hash => postgresql_password('marat', 'vashepohui'),
    }

    postgresql::server::role { 'valera':
        password_hash => postgresql_password('valera', 'ayavalera'),
    }


    class { 'mongoose':
        host       => 'ec2-54-183-152-193.us-west-1.compute.amazonaws.com',
        dbhost     => 'localhost',
        dbname     => 'spring_fruit_production',
        dbport     => '5432',
        dbuser     => 'sf',
        dbpassword => 'loveyourdoughter'
    }


}


