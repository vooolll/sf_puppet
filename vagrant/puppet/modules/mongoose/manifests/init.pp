class mongoose($host, $dbhost, $dbport, $dbname, $dbuser, $dbpassword) {

    $path_to_default_schema = '/vagrant/sf_chat/apps/ejabberd/priv/pg.sql'
    exec { "clone":
        user => root,
        onlyif => "test ! -d /vagrant/sf_chat",
        command => 'git clone git@bitbucket.org:vooolll/sf_chat.git /vagrant/sf_chat',
        path => ["/bin", "/usr/bin"],
    } -> file { "edit-config":
        path    => "/vagrant/sf_chat/rel/reltool_vars/odbc_redis_vars.config",
        content => template('mongoose/odbc_redis_vars.config'),
    } -> package { "erlang":
        ensure => latest,
        require => Exec["aptGetUpdate"],      
    } -> package { "expat":
        ensure => latest,
        require => Exec["aptGetUpdate"],
    } -> package { "libexpat1-dev":
        ensure => latest,
        require => Exec["aptGetUpdate"],
    } -> exec { "rm deps":
        cwd => '/vagrant/sf_chat',
        command => 'rm -rf deps',
        path => ["/bin", '/usr/bin'],
    } -> exec { "chat-make":
        environment => "HOME=/root",
        cwd => '/vagrant/sf_chat',
        command => 'make',
        path => ["/bin", '/usr/bin'],
    } -> exec { "chat-make-devrel":
        environment => "HOME=/root",
        cwd => '/vagrant/sf_chat',
        command => 'make devrel',
        path => ["/bin", '/usr/bin'],
    }



}
